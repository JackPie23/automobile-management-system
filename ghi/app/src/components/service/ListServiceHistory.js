import { useState, useEffect } from "react";

export default function ListServiceHistory() {
	const [serviceAppointments, setServiceAppointments] = useState([]);
	const [automobiles, setAutomobiles] = useState([]);
	const [search, setSearch] = useState("");

	// event handlers
	const handleSearchChange = (event) => {
		setSearch(event.target.value);
	};

	// initial data pull from appointments and automobiles on component mount
	const getData = async () => {
		const url = "http://localhost:8080/api/appointments/";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setServiceAppointments(data.appointments);
		}

		const url2 = "http://localhost:8100/api/automobiles/";
		const response2 = await fetch(url2);

		if (response2.ok) {
			const data2 = await response2.json();
			setAutomobiles(data2.autos);
		}
	};

	// useEffect to run getData on component mount
	useEffect(() => {
		getData();
	}, []);

	// Format date and time from ISO string
	const formatDateTime = (isoString) => {
		const dateTime = new Date(isoString);
		const date = dateTime.toLocaleDateString();
		const time = dateTime.toLocaleTimeString();
		return { date, time };
	};

	// Filter service appointments based on the search input
	const filteredServiceAppointments = serviceAppointments.filter(
		(appointment) =>
			appointment.vin.toLowerCase().includes(search.toLowerCase())
	);

	return (
		<>
			<h1>Service History</h1>
			<div className="container">
				<div className="row">
					<div className="col-11">
						<input
							type="text"
							className="form-control"
							id="search"
							value={search}
							onChange={handleSearchChange}
							placeholder="Search by VIN..."
						/>
					</div>
					<div className="col-1">
						<button
							className="btn btn-success btn-block"
							onClick={handleSearchChange}
						>
							Search
						</button>
					</div>
				</div>
			</div>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>is VIP?</th>
						<th>Customer</th>
						<th>Date</th>
						<th>Time</th>
						<th>Technician</th>
						<th>Reason</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					{/* map over the filtered appointments */}
					{filteredServiceAppointments.map((appointment) => {
						// create variable for technician's full name
						const technicianFullName = `${appointment.technician.first_name} ${appointment.technician.last_name}`;

						// Format the date and time
						const { date, time } = formatDateTime(
							appointment.date_time
						);

						// Find the corresponding automobile
						const automobile = automobiles.find(
							(auto) => auto.vin === appointment.vin
						);

						return (
							<tr key={appointment.id} value={appointment.id}>
								<td>{appointment.vin}</td>
								<td>
									{/* from the automobile list, if the automobile is sold, display "Yes", otherwise display "No" */}
									{automobile
										? automobile.sold
											? "Yes"
											: "No"
										: "No"}
								</td>
								<td>{appointment.customer}</td>
								<td>{date}</td>
								<td>{time}</td>
								<td>{technicianFullName}</td>
								<td>{appointment.reason}</td>
								<td>{appointment.status}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
}

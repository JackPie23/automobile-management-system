import React, { useState, useEffect } from "react";

export default function ListTechnicians() {
	const [technicians, setTechnicians] = useState([]);

	// initial data pull from technicians on component mount
	const getData = async () => {
		const url = "http://localhost:8080/api/technicians/";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setTechnicians(data.technicians);
		}
	};

	// useEffect to run getData on component mount
	useEffect(() => {
		getData();
	}, []);

	return (
		<>
			<h1>Technicians</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Employee ID</th>
						<th>First Name</th>
						<th>Last Name</th>
					</tr>
				</thead>
				<tbody>
					{/* map through the technicians array and display each technician in a row */}
					{technicians?.map((technician) => (
						<tr key={technician.id} value={technician.id}>
							<td>{technician.employee_id}</td>
							<td>{technician.first_name}</td>
							<td>{technician.last_name}</td>
						</tr>
					))}
				</tbody>
			</table>
		</>
	);
}

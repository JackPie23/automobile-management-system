import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

export default function CreateModel() {
	const navigate = useNavigate();
	const [name, setName] = useState("");
	const [picture_url, setPicture_url] = useState("");
	const [manufacturer, setManufacturer] = useState("");
	const [manufacturers, setManufacturers] = useState([]);

	const handleNameChange = (event) => {
		setName(event.target.value);
	};

	const handlePicture_urlChange = (event) => {
		setPicture_url(event.target.value);
	};

	const handleManufacturerChange = (event) => {
		setManufacturer(event.target.value);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {
			name: name,
			picture_url: picture_url,
			manufacturer_id: manufacturer,
		};
		const url = "http://localhost:8100/api/models/";
		const fetchConfig = {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify(data),
		};

		const response = await fetch(url, fetchConfig);
		if (response.ok) {
			const data = await response.json();
			setManufacturers([...manufacturers, data]);

			setName("");
			setPicture_url("");
			setManufacturer("");

			navigate("/models");
		}
	};

	const getData = async () => {
		const url = "http://localhost:8100/api/manufacturers/";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setManufacturers(data.manufacturers);
		}
	};

	useEffect(() => {
		getData();
	}, []);

	return (
		<>
			<div className="row">
				<div className="offset-3 col-6">
					<div className="shadow p-4 mt-4">
						<h1>Create a vehicle model</h1>
						<form onSubmit={handleSubmit}>
							<div className="form-floating mb-3">
								<input
									value={name}
									onChange={handleNameChange}
									placeholder="Name"
									required
									type="text"
									name="name"
									id="name"
									className="form-control"
								/>
								<label htmlFor="name">Name</label>
							</div>
							<div className="form-floating mb-3">
								<input
									value={picture_url}
									onChange={handlePicture_urlChange}
									placeholder="Picture URL"
									required
									type="text"
									name="picture_url"
									id="picture_url"
									className="form-control"
								/>
								<label htmlFor="picture_url">Picture URL</label>
							</div>
							<div className="mb-3">
								<select
									value={manufacturer}
									onChange={handleManufacturerChange}
									required
									name="manufacturer"
									id="manufacturer"
									className="form-select"
								>
									<option value="">
										Select a manufacturer
									</option>
									{manufacturers?.map((manufacturer) => (
										<option
											key={manufacturer.id}
											value={manufacturer.id}
										>
											{manufacturer.name}
										</option>
									))}
								</select>
							</div>
							<button type="submit" className="btn btn-success">
								Create
							</button>
						</form>
					</div>
				</div>
			</div>
		</>
	);
}

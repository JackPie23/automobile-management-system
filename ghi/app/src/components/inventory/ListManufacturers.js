import { useState, useEffect } from "react";

export default function ListManufacturer() {
	const [manufacturers, setManufacturers] = useState([]);

	const getData = async () => {
		const url = "http://localhost:8100/api/manufacturers/";
		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setManufacturers(data.manufacturers);
		}
	};

	useEffect(() => {
		getData();
	}, []);

	return (
		<>
			<div>
				<h1>Manufacturers</h1>
			</div>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Name</th>
					</tr>
				</thead>
				<tbody>
					{manufacturers?.map((manufacturer) => (
						<tr key={manufacturer.id} value={manufacturer.id}>
							<td>{manufacturer.name}</td>
						</tr>
					))}
				</tbody>
			</table>
		</>
	);
}

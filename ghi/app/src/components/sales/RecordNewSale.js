import React, {useEffect, useState} from 'react'
import {useNavigate} from 'react-router-dom'
function RecordSale(){
    const navigate = useNavigate();
    const [autos, setAutos] = useState([]);
    const [salespeople, setSalesPeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesPerson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');
    
    

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value)
    }
    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value)
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value)
    }
    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value)
    }

    const getAutomobileData = async () => {
        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const response = await fetch(automobileUrl)
        if (response.ok){
            const data = await response.json();
            
            setAutos(data.autos)
            
        }
    }
    const getPersonData = async () => {
        const salespersonUrl = "http://localhost:8090/api/salespeople/";
        const response = await fetch(salespersonUrl)
        if(response.ok){
            const data = await response.json();
            
            setSalesPeople(data.salespeople)
        }
    }
    const getCustomerData = async () => {
        const customerUrl = "http://localhost:8090/api/customers/";
        const response = await fetch(customerUrl)
        if(response.ok){
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(() => {
        getAutomobileData();
        getCustomerData();
        getPersonData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            automobile: automobile,
            salesperson: salesperson,
            customer: customer,
            price: price,
            
            
        }
        
        const salesUrl = "http://localhost:8090/api/sales/";
        const fethConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch (salesUrl, fethConfig);
        if (response.ok){

            setAutomobile('');
            setSalesPerson('');
            setCustomer('');
            setPrice('');
            const automobileUrl = `http://localhost:8100/api/automobiles/${automobile}/`;
            const automobileData = { sold: true};
            const automobilefetchConfig = {
                method: 'put',
                body: JSON.stringify(automobileData),
                headers: {
                    "Content-Type": "application/json",
                }
            }
            await fetch(automobileUrl, automobilefetchConfig)
            getAutomobileData();
            navigate("/sales")
            
            
            
        }
    }
    return (
        <div className='container'>
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create A New Sale</h1>
                <form onSubmit={handleSubmit} id="create-sale-form">
                    <div className='form-floating mb-3'>
                        <select value={automobile} onChange={handleAutomobileChange} required name="automobile" id="automobile" className='form-select'>
                            <option value=''>Choose An Automobile</option>
                            {autos.filter(automob => automob.sold === false).map(automob => {
                                return (
                                    <option key={automob.vin} value={automob.vin}>
                                        {automob.vin}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <div className='form-floating mb-3'>
                    <select value={salesperson} onChange={handleSalesPersonChange} required name="salesperson" id="salesperson" className='form-select'>
                            <option value=''>Choose A Salesperson</option>
                            {salespeople.map(salesperson => {
                                return (
                                    <option key={salesperson.employee_id} value={salesperson.employee_id}>
                                        {`${salesperson.first_name} ${salesperson.last_name}`}
                                    </option>
                                )
                            })}
                        </select>
                        
                    </div>
                    <div className='form-floating mb-3'>
                    <select value={customer} onChange={handleCustomerChange} required name="customer" id="customer" className='form-select'>
                            <option value=''>Choose A Customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>
                                        {`${customer.first_name} ${customer.last_name}`}
                                    </option>
                                )
                            })}
                        </select>
                        
                    </div>
                    <div className='form-floating mb-3'>
                        <input value={price} onChange={handlePriceChange} placeholder="Price" required type="int" name="price" id="price" className="form-control"/>
                        <label htmlFor="price">Price</label>
                    </div>
                    <div>
                        <button className='btn btn-success'>Create</button>
                    </div>
                    
                </form>
            </div>
            </div>
        </div>
        </div>
    )
}

export default RecordSale
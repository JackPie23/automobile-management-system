import {useEffect, useState} from 'react'

function CustomerList(){
    const [customers, setCustomers] = useState([]);
    const fetchCustomer = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            
            setCustomers(data.customers)
        }
    }
    useEffect(() => {
        fetchCustomer()
    }, [])

    return(
        <div className='h-screen'>
            <br></br>
            <h1>Customers</h1>
            <br></br>
            <div>
                <table className="table table-striped">
                    <thead >
                        <tr>
                            <th>ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {customers.map(customer => {
                            return(
                                <tr key={customer.id} value = {customer.id}>
                                    <td>{ customer.id }</td>
                                    <td>{ customer.first_name }</td>
                                    <td>{ customer.last_name }</td>
                                    <td>{ customer.address }</td>
                                    <td>{ customer.phone_number }</td>
                                    
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>

        </div>
    )
}

export default CustomerList
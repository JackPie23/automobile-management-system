import React, {useState} from 'react'
import {useNavigate} from 'react-router-dom'
function CreateSalesPerson(){
    const navigate = useNavigate();
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    
    const handleFirstNameChange = (e) => {
        const value = e.target.value;
        setFirstName(value)
    }
    const handleLastNameChange = (e) => {
        const value = e.target.value;
        setLastName(value)
    }
    const handleEmployeeIdChange = (e) => {
        const value = e.target.value;
        setEmployeeId(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            first_name: firstName,
            last_name: lastName,
            employee_id: employeeId
        }
        const salespeopleUrl = "http://localhost:8090/api/salespeople/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            }
        }
        const response = await fetch(salespeopleUrl, fetchConfig)
        if(response.ok){
            setEmployeeId('');
            setFirstName('');
            setLastName('');
            navigate("/salespeople")
        }
    }
    return (
        <div className='container'>
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create A New Salesperson</h1>
                <form onSubmit={handleSubmit} id="create-customer-form">
                    <div className='form-floating mb-3'>
                        <input value={firstName} onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="firstname" id="firstname" className="form-control"/>
                        <label htmlFor="firstname">First Name</label>
                    </div>
                    <div className='form-floating mb-3'>
                        <input value={lastName} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="lastname" id="lastname" className="form-control"/>
                        <label htmlFor="lastname">Last Name</label>
                    </div>
                    <div className='form-floating mb-3'>
                        <input value={employeeId} onChange={handleEmployeeIdChange} placeholder="Employee Id" required type="text" name="employeeId" id="employeeId" className="form-control"/>
                        <label htmlFor="employeeId">Employee Id</label>
                    </div>
                    <div>
                        <button className='btn btn-success'>Create</button>
                    </div>
                    
                </form>
            </div>
            </div>
        </div>
        </div>
    )
    
}

export default CreateSalesPerson
import {useState, useEffect} from 'react'

function ListSales(){
    const [sales, setSales] = useState([])

    const fetchSales = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setSales(data.sales)
        }
    }
    useEffect(() => {
        fetchSales()
    }, [])

    return (
        <div className='h-screen'>
        <br></br>
        <h1>List of Sales</h1>
        <br></br>
        <div>
            <table className="table table-striped">
                <thead >
                    <tr>
                        <th>Employee Id</th>
                        <th>Staff Name</th>
                        <th>Customer Name</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return(
                            <tr key={sale.id} value = {sale.id}>
                                <td>{ sale.salesperson.employee_id }</td>
                                <td>{ `${sale.salesperson.first_name} ${sale.salesperson.last_name} `}</td>
                                <td>{ `${sale.customer.first_name} ${sale.customer.last_name}` }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>{ `$${sale.price}.00 `}</td>
                                
                                
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>

    </div> 
    )
}

export default ListSales
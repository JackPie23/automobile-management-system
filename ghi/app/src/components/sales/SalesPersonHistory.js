import { useState, useEffect } from "react";

function SaleHistory(){
    const [sales, setSales] = useState([])
    const [salespeople, setSalesPeople] = useState([])
    const [salesperson, setSalesPerson] = useState('')

    const handleSalesPersonChange = (event) => {
        const value = event.target.value
        setSalesPerson(value)
    }

    const getSales = async () => {
        const salesUrl = "http://localhost:8090/api/sales/"
        const response = await fetch(salesUrl)
        if (response.ok){
            const data = await response.json();
            setSales(data.sales)
        }
    }
    const getSalesPeople = async () => {
        const salesPeopleUrl = "http://localhost:8090/api/salespeople";
        const response = await fetch(salesPeopleUrl)
        if (response.ok){
            const data = await response.json();
            setSalesPeople(data.salespeople)
        }
    }
    useEffect(() => {
        getSales();
        getSalesPeople();
    }, [])

    return(
        <div className='h-screen'>
        <br></br>
        <h1>Sales History</h1>
        <br></br>
        <div>
            <select value={salesperson} onChange={handleSalesPersonChange} required name="salespeople" id="salespeople" className="form-select">
                <option value="">Choose A Salesperson</option>
                {salespeople.map(salesperson => {
                    return (
                        <option key={salesperson.employee_id} value={salesperson.employee_id}>
                            {`${salesperson.first_name} ${salesperson.last_name}`}
                        </option>
                    )
                })}
            </select>
            <table className="table table-striped">
                <thead >
                    <tr>
                        <th>Salesperson</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.filter((sale) => sale.salesperson.employee_id === salesperson).map(sale => {
                        return(
                            <tr key={sale.id} value = {sale.id}>
                                <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
                                <td>{ `${sale.customer.first_name} ${sale.customer.last_name}` }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>{ `$${sale.price}.00 `}</td>
                                
                                
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>

    </div> 
    )
}

export default SaleHistory